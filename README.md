# LHeC CDR 2019

Thanks for contributing to the 2019 CDR for the LHeC experiment and accelerator.
Here, we briefly provide instructions for the editors of the CDR document in order
to facilitate editing.

Quick start
-----------
 +  Clone the git repository, e.g.: \
      `git clone https://<CERN-USERNAME>@gitlab.cern.ch/lhec/lhec-cdr-2019.git` \
    See https://gitlab.cern.ch/lhec/lhec-cdr-2019 for alternative connection details (click 'Clone' at top-right.)
 +  Go to the respective (sub)directory, i.e.:\
      `cd lhec-cdr-2019/higgs`
 + 'compile' the selected chapter, by typing \
      ` make` \
    (which just calls `pdflatex` and `bibtex` consecutively)
 + Open the resulting PDF file, e.g. \
   `open higgs.pdf`

For editing, just insert your contribution to the respective .tex-file (e.g. higgs.tex)
(note: since there may be a more distinct tex-substructure for some chapters,
please look out for further .tex-files in the directory).

Note
--------------
Every section can be 'compiled' separately, when calling `make` (or manually
`pdflatex [...]` and `bibtex [...]`) in the respective subdirectory.
The entire document is generated, when doing so in the directory `./main`.

Commit changes
--------------
Before commiting changes, please ensure that you have updated the repo,
and you can compile the document
  + `git pull`                             synchronise with latest version
  + [perform your changes...]             
  + `git pull`                             synchronise with latest version
  + `make`                                 Important. Test compilation before commit!
  + `git status`                           See, what you have changed. You may want to cleanup before `make clean`
  + `[git add <filenames>]`                if needed, add new files. Mind, do not add 'auto-generated' files!
  + `git commit -m "<message>" <files>`    Commit changes
  + `git push`                             push your contribution to the repository

No CERN account
---------------
In case you do not have a CERN computing account:
Clone/checkout the gitlab repository or download the source code as
zip-file or tar-ball from https://gitlab.cern.ch/lhec/lhec-cdr-2019.
Then make your edits prompt, and send your contribution to the
respective chapter editors by mail.


Further instructions
--------------------
Further instructions are included in each chapter of the document,
once you have checked out the source, and generated the pdf file.



Invitation as sent by mail (Remarks on the 'LHeC at HL-LHC' Paper)
------------------------------------------------------------------

1. The paper should be an update of the CDR, may refer to that, but also be selfconsistent. It will have a few hundred pages, may be 400. There is no direct page limit, neither in total nor for any chapter. It will be published in JPhysG.

2. We will use PDFLaTeX and git, such that all may directly edit.

3. For release in the fall, for presenting the results at the Chavannes workshop https://indico.cern.ch/event/835947/, and for having a bit of time for editing, we have set a deadline of 11.10.2019 for all contributions. As all know, deadlines tend to slip, we yet will have to make a sincere effort to release the paper to the arXiv in November, for which 11.10. looks just about realistic. It is known to be tight, but we all write about things we have been working on for long.

4. There have been chapters created and chapter editors invited, who kindly agreed to help bringing the chapters together. Nothing is frozen, additional names/colleagues may be invited, headlines be changed as writing will dictate/suggest. This mail is to all of you, the authors of sections and editors who surely will find a good way to collaborate. The overall editing will be with Oliver and Max

5. We have agreed to write an update on LHeC at HL-LHC, not the FCC as its CDR just went out. Where reasonable a link to FCC as well as joint presentations or plots may be instructive. We thought it would be interesting, as an Appendix, to have a separate chapter on ep with what now is called LE FCC, a 20 TeV proton energy FCC.

6. We have put more emphasis than before on the relation to pp. Thus there is a separate chapter on HL-LHC and a separate chapter on the relation of ep with pp. we thought emphasis should also be clear to the importance of eA.

7. Further, the importance of energy recovery and the role and perspective of PERLE must be disussed, this is currently an appendix, but represents the base of the accelerator development to some extent.

8. Following the cost estimates and IR synchroron radiation laod, we consider Ee=50 GeV in 1/4 U(LHC) as a new baseline [compared to 60 GeV, 1/3]. The 1/4 will allow upgrades to almost 60 GeV and we therefore shall not aim at redoing all analyses done with 60 GeV now with 50. If you do new ones, take in doubt 50 GeV please.
